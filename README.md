# Liberation Philly Bylaws

Contained in this repo are the source files for [Liberation Philly's bylaws](https://lp-bylaws.surge.sh/).

## License

The text in this repo is licensed under [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) and should be attributed to *Liberation Philly*. Anyone who makes contributions to the Liberation Philly Bylaws is implicitly licensing their work under CC-BY-SA 4.0 and agrees to be attributed as *Liberation Philly*.

## Contributing

The files contained in `book` are all just markdown files, so if you have a markdown editor that should be enough to preview your changes. You can also navigate directly to the file on GitLab and click the edit button to create a pull request directly in the browser.

Setting up the whole environment locally requires Nodejs to be installed. You may do it like so:

```bash
$ npm install
$ npm run start
```

Similarly, with proper deployment credentials, one may run:

```bash
$ npm run deploy
```
