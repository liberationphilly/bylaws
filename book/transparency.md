# Transparency

Transparency is important because a monopoly on information creates a hierarchy between those who have access, and those who do not. It seeds mistrust among community members, and between the organization and the outside world.

**Transparency should be considered especially in the following areas:**

1. How and why decisions are made
2. Financial information, including fundraising and expenses
3. Mistakes and failures to live up to our Values
4. The processes through which we have found success
