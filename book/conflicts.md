# Conflict Resolution

## 1. Purpose of the Group

The purpose of the Conflict Resolution (CR) group is to provide a resource to the Liberation Philly community for peacefully resolving both interpersonal conflict among members, as well as facilitating conflict resolution with outside individuals or groups. The goal of the CR group should be to restore the beloved community and heal harms that have been done. The CR group does not dole out punishment, but rather facilitates communication and compromise, leads restorative justice processes, suggests outside resources, and/or facilitates peaceful exits from the group (when appropriate).

## 2. CR group responsibilities

* Being an unbiased and trusted resource and facilitator for the community on the topics of: non-violent communication, conflict resolution, mediation, restorative justice, and negotiation
* Developing and publicizing a detailed step-by-step process for internal conflict resolution
* Providing resources and training for conflict prevention within Liberation Philly
* Supporting the relevant parties involved in a conflict, assuming good faith, and being willing to teach and heal rather than judge and punish
* Managing each conflict in a timely manner
* Respecting the confidentiality of individuals involved in a conflict (unless otherwise agreed)
* Facilitating communication with other AR groups when conflict with them arises
* Developing a list of outside resources and protocols for when the CR group is unequipped to handle the conflict (ex. cases of gross misconduct)
* Scheduling trainings on relevant topics for Liberation Philly Members and for itself, and/or provide resources to supplement trainings.

## 3. Transparency

The Conflict Resolution Group would be a special operations group, operating outside of the standard Working Group order (as referenced in the bylaws). This is due to the sensitive and confidential nature of managing interpersonal conflict, as well as mandatory special skills and experience required to participate in a conflict resolution process. Additionally, members of a CR will only be able to successfully lead many of the processes required if they are universally trusted and respected among members.

The CR team’s members should be publicized, and documents which outline the group’s processes and procedures should be accessible through the bylaws. Special efforts to be transparent should be made for certain conflicts that may affect the larger community or movement.

## 4. Conflict Resolution Process

As noted above, the CR group will be responsible for developing a detailed guide to resolving member conflicts. These are the general steps that the group may take, with certain steps being more or less appropriate for any given conflict:

1. **Calling in** – Educating an individual that their behavior does not meet the group’s values or culture guidelines and providing them resources to grow.

2. **Warning** – Clarifying for a second time that they individual’s behavior is unacceptable to the group and asking them to adjust going forward.

3. **Mediation** – An unbiased, facilitated discussion between two or more parties in conflict with each other, with the goal of coming to a mutual understanding, healing and growing the relationship, and finding a mutually-acceptable solution for all.

4. **Restorative Justice** – The process of providing both a) healing to a party who has been hurt in some way by another, and b) opportunities for growth, learning, and accountability to the transgressing party.

5. **Referral** – In certain circumstances, the CR group may be unequipped to handle a conflict. They may refer the conflicted parties to an outside resource, and, as appropriate, require successful completion of any number of steps by a conflicted party prior to returning to the community.

6. **Removal** – In cases of gross misconduct, repeated refusal to respect warnings, unwillingness to participate in an RJ process, and/or other cases of equal severity, the CR group may recommend and facilitate (through a process they should determine and publicize) the removal of an individual from the Liberation Philly community. This requires consensus of the entire CR group.

## 5. Training

Prior to joining the CR group, each member is required to undergo training in the following areas:

* Non-violent communication
* Mediation
* Restorative Justice
* Diversity
* Sexual assault and consent

**Probationary members:** If there is a capacity shortage, members may be added to the CR group on a probationary basis pending the completion of trainings. Probationary members may conduct a CR process only with the supervision of a fully-trained member. Trainings should be held as often as possible to allow the largest number of people to become qualified.

The CR group may permit a certain resource (ex. a recorded training or book) to be used in place of formal training on a case-by-case basis. This should be used sparingly.

The CR group is permitted to request funding from AWG to support trainings for its members or the group as deemed appropriate. Effort should be made to provide affordable training options.

## 6. Adding/Removing Members

Members are added to the CR group through a nomination process, including for the initial creation of the CR team. Any group member may nominate another. Self nominations are not permitted. After someone is nominated, the entire group must form consensus to allow the individual onto the CR group. Any block must be accompanied by a specific action item the individual can take to be deemed qualified (ex. resolve an open conflict, complete a training, or participate more actively in the group).

Members of the CR group may be removed either by a) consensus of all members outside of the CR group, or b) consensus of all other members within the CR group.
