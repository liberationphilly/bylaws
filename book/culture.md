# Culture

## Radical Feminism

We are a radical feminist group that prioritizes women. We are tired of women being ignored, silenced, and abused within the animal rights movement. Male violence and those who bring it are not tolerated in our space.

## Anti-Racism

We do not tolerate racism in our group, and require consideration of how any actions we may take may disproportionately affect communities of color. We recognize that we have an obligation to provide resources to these communities especially.

## Against Human Exploitation

We do not support the exploitation of humans in either our current system, or in any future system regardless of animal liberation. We recognize that capitalism leads to exploitation and abuse of workers, and support efforts to collectivize work places. We recognize that many popular "vegan" brands of food, clothing, cosmetics, etc come at the cost of human exploitation. We do not support this exploitation, and do not engage in consumer-centered advocacy for these reasons.

## Environmental Impact

We make an effort to leave the smallest footprint we can on this planet, and take the environmental impact of our actions into account.

## Freedom-respecting

We respect the freedom of all individuals, and thus do not require the use of proprietary software for participation within the group. We encourage all of our members to use free and secure software, and are willing to take extra steps to ensure that this is possible for all.
