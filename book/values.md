# Values

Liberation Philadelphia is a grassroots animal rights group. You can learn more about us on [our website](https://liberationphilly.org).

## Our mission

Liberation Philly is an abolitionist group dedicated to total liberation for all animals. We use a combination of direct action tactics including bearing witness, protest, civil disobedience and rescue with the short-term goal of putting the issue of animal rights "on the table" such that it can no longer be ignored.

## Group values

1. **Non-Violence.** We never use violence. We respond to violence with restraint and compassion. We use non-violent communication with each other and with others.
2. **Anti-Speciesism.** All individuals deserve the same moral consideration regardless of species. We do not support speciesist beliefs, actions, or language.
3. **Non-Hierarchy.** Every member has equal decision making power.
4. **Transparency.** Every member of the group should have access to the decisions we've made. A monopoly on information creates a monopoly on decision making power. We should be transparent in our operations to the outside world, giving them no reason to mistrust us.
5. **Amplifying Marginalized Voices** While racism and sexism pervade both society at large and the animal rights movement, we aim to create a space which prioritizes the safety and voices of women, people of color, and all marginalized communities. Our women-centered spaces and events empower women to shape the anti-speciesist narrative through a radical feminist framework and form connections with other female leaders. We strive to work with marginalized communities to ensure our message is not contributing to the oppression of any other group of people.
6. **Radical Candor.** Radical Candor is the ability to Challenge Directly and show you Care Personally at the same time. Radical Candor will help you and all the people you work with do your best work while facilitating relationship growth among comrades.
