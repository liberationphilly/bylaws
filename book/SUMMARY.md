# Summary

* [Introduction](README.md)
* [Our values](values.md)
* [Organizational Structure](structure.md)
* [Making Decisions](decisions.md)
* [Transparency](transparency.md)
* [Funding](funding.md)
* [Culture](culture.md)
* [Conflict Resolution](conflicts.md)
