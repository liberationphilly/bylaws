# Liberation Philly Bylaws

These are the bylaws for Liberation Philly, a grassroots animal rights organization. Bylaws may be changed through the decision making process outlined in [Making Decisions](decisions.md).

## Contents

* [Our values](values.md)
* [Organizational Structure](structure.md)
* [Making Decisions](decisions.md)
* [Transparency](transparency.md)
* [Funding](funding.md)
* [Culture](culture.md)
* [Conflict Resolution](conflicts.md)
