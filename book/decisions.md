# Making Decisions

Decisions are made through consensus.

## About Forming Consensus

> **Consensus decision making is a creative and dynamic way of reaching agreement between all members of a group. Instead of simply voting for an item and having the majority of the group getting their way, a group using consensus is committed to finding solutions that everyone actively supports, or at least can live with.** This ensures that all opinions, ideas and concerns are taken into account. Through listening closely to each other, the group aims to come up with proposals that work for everyone.

> By definition, in consensus no decision is made against the will of an individual or a minority. If significant concerns remain unresolved, a proposal can be blocked and prevented from going ahead. This means that the whole group has to work hard at finding solutions that address everyone's concerns rather than ignoring or overruling minority opinions.

> —[Seeds For Change](https://www.seedsforchange.org.uk/consensus)

### Threshold of Relevancy

Consensus is only required from people who are directly affected by a decision, or from all members when it affects either 1) the safety of the members or 2) the strategy of the organization on a large scale.

If the threshold of relevancy is not reached, consensus from that specific individual is not required. However, the input of all members into any decision, relevant or not, should always be taken very seriously.

## Logistics

During working group or AWG meetings, consensus may be formed by simply posing a proposal and requesting a consensus. Any individual(s) blocking a proposal are expected to propose an amendment or alternate proposal which will allow the group to move forward.

Decisions made by a working group should be transparent to all members, and may be blocked by any member who meets the threshold of relevancy.

When making a decision where all members should be involved, a post should be made in the group chat explaining the proposal and include a link to a poll. All effort should be made to encourage 100% participation in the poll.

Votes are always expected to fall within the defined values, missions, and tactics of Liberation Philadelphia. The exception to this is on a vote to change the values, mission, or tactics. Votes which do not fall within our values (ex. a vote to use violence or enable a hierarchy), may be considered void.


## Forming Quorum

For forming consensus on a matter which requires the entire group, 25% of the group must participate in the process (rounded down to the nearest whole person). For the sake of this metric, and this metric only, the "entire group" will be defined as the people in the Riot chat room.

For forming consensus at an All Working Groups meeting, quorum is defined as 50% of regular members.  For the sake of this metric, and this metric only, the "regular members" will be defined as the people in the AWG Riot chat room.

For forming consensus in a working group, quorum is 75% of participating members (defined as members who regularly attend that WG's meetings - "regular" may be further defined by individual WGs).


## Exceptions

Special Ops groups, as defined in [Organizational Structure](structure.md), may operate and make decisions autonomously and according to what they decide is best. For example, a rescue mission team may designate a team leader for when calls need to be made quickly.

Additionally, in the case of a time-sensitive or unsafe situation, members are permitted to make one-time executive decisions. Any member may do this when appropriate, however the following considerations should be taken into account:

1. Does this individual have the necessary skills, experience, and knowledge to be making this decision in this moment?

2. Is there any way to make this decision more open, even if it involves bringing in only a small number of people for now?

3. After an executive decision is made, what steps will be done to be transparent with the group and rectify any problems that may arise due to this decision?

Executive decisions should be only made in the rarest cases.
