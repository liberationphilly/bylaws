# Organizational Structure

While being non-hierarchical, there is still a need for organization and division of labor to accomplish our goals. This is how we divide labor.

## Liberation Philly Members

Anyone who has attended one Liberation Philly event within the past 6 months may be considered a member, and should have access to our group discussions, working groups, and decision making process.

## Working Groups

A working group is a subset of the general population of members who are accountable to the rest of the group for a specific range of tasks. Each WG should meet regularly (generally weekly or bi-weekly), and use consensus within their group for their own decisions.

While working groups will make the majority of the decisions related to their tasks autonomously, they are accountable to all members for accomplishing what they say they will. All members have the ability to form consensus to override any specific WG's decision at any point.

Working groups should be open to any member, regardless of skills or experience, and willing to teach and promote emergent leadership within their group.

**Current Working Groups Include:**

* Protest
* Anonymous for the Voiceless
* Philly Farmed Animal Save
* Outreach
* Tech
* Art
* Social Media
* Animal Care
* Community Outreach
* Environmental Impact

## All Working Groups

Once a week, members of each working group should come together to discuss the current goings-on, areas where help or improvement is needed, and big-picture planning. AWG meetings are open to all members, as well, and the happenings of these meetings should be accessible to any interested member.

## Special Operations Groups

Some groups may form independently of the AWG structure for the purpose of security culture, or necessity. A Special Ops group is once which, due to necessity, may not be transparent with the rest of the group or with the outside world. For example, certain rescue teams may require a lack of transparency prior to the completion of their mission for the safety of the humans and non-humans involved, or a future legal team may be legally required to keep certain information confidential. A conflict resolution team may also require certain information to be kept confidential.

In the case of the formation of a Special Ops group, efforts should be made to provide as much transparency to the general group as soon as possible. Often the group should be notified of the formation of the group and it's members, even if not made fully aware of the specific cases they are handling (ex. conflict res and legal.)
