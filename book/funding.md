# Financial Information

## Storage of and Access to LP Funds

A Liberation Philly bank account should be created to which a member may gain card-holder access after meeting the following requirements:

1. Has been a regular member of at least one working group for 1 year. "Regular" may be defined as attending at least 75% of that working group's meetings and events, as well as AWG meetings.
2. AWG Members agree by consensus that this individual should be a card-holder

**Revoking card-holder access:**

Card holder access will be automatically revoked in the following circumstances:

1. The card-holder no longer qualifies as a "regular" member of a working group
2. The card-holder has been removed from the Liberation Philly community due to a violation of these bylaws
3. The card-holder has violated the expense approval process 3 times in 1 year.

Automatic revocation may be contested and overruled by consensus of AWG members.

At any time AWG members may form consensus to revoke an individual's card access due to violation of trust or community values, fiscal irresponsibility, or conflict of interest. In these cases, consensus from the member in question is not required.


## Fundraising Guidelines

Fundraising may be done through a variety of methods including:

- Facebook fundraisers
- Crowdsourcing platforms
- LiberationPhilly.org website
- In person
- Through grants or bequests
- Through events
- Through merchandise sales
- Through auctioning of goods or services

Grant from other organizations should be considered carefully in the following ways:

- What strings does this money come with? What do they require in return?
- Would we be comfortable publicizing that we have accepted money from this group? How will this shape public opinion of us?
- Does accepting this money influence or change our plans in any particular way?
- Is this money is any way expected to prevent us from taking certain actions?

## Spending Guidelines

All spending that exceeds the should be proposed through a written form to AWG during weekly meetings. Expense requests may only be approved through AWG consensus. Non-cardholders may request reimbursement for expenses or temporary access to a card through the same process. Receipts are required for all purchases.

Special Ops groups may request funds by submitting the form to only current card-holders, and requesting a transparency hold for a specific amount of time. After the transparency hold is complete, full transparency in expenses should be publicized.

**Exceptions:**
Card-holders may make purchases totaling $25 or less per month without requiring approval.

## Financial Transparency

- Books should be published quarterly and posted as a PDF on our website.
- All donations and/or grants should be publicly listed, names may be withheld to protect identities if requested.
- All expenses should be itemized. Specific items may be redacted if the publication is occurring during a transparency hold.
- After transparency holds are over, updated records should be made public.
- Who current card-holders are should be public.
